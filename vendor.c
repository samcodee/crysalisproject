#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

void sleep(int number_of_seconds)
{
    // Converting time into milli_seconds
    int milli_seconds = 1000 * number_of_seconds;

    // Storing start time
    clock_t start_time = clock();

    // looping till required time is not achieved
    while (clock() < start_time + milli_seconds)
        ;
}

void clear()
{
    system("cls");
}

void main()
{
    char selectedItem, selectedPayment, option;
    printf("\"The C Vending Machine\" - Made by samcodee & team for Chrysalis day 2022-2023\n");
    sleep(2);
    printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| | Products                 |   |\n| | 1) Dairy Milk            |   |\n| | 2) Sprite                |   |\n| | 3) Lays                  |   |\n| | 4) Doritos               |   |\n| | 5) Random Item           |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
    selectedItem = getch();
    clear();
    printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| | Selected Item - 1        |   |\n| |                          |   |\n| | Payment method:          |   |\n| | 1) Coins                 |   |\n| |                          |   |\n| | 2) Magic                 |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
    selectedPayment = getch();
    clear();
    if (selectedPayment == '1')
    {
        printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| | Selected Item - 1        |   |\n| | Selected payment method: |   |\n| | COINS                    |   |\n| |                          |   |\n| | Confirm purchase?        |   |\n| |       [Y/N]              |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
        getch();
        clear();
        printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| | Thanks for purchasing!   |   |\n| |                          |   |\n| | Collect your Item down   |   |\n| | below                    |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
        sleep(2);
        return;
    }
    else if (selectedPayment == '2')
    {
        printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| | Selected Item - 1        |   |\n| | Selected payment method: |   |\n| | MAGIC                    |   |\n| |                          |   |\n| | Confirm purchase?        |   |\n| |       [Y/N]              |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
        getch();
        clear();
        printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| | Selected Item - 1        |   |\n| | Selected payment method: |   |\n| | MAGIC                    |   |\n| |                          |   |\n| | Answer the next few      |   |\n| | questions to dispense    |   |\n| | items for free!          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
        getch();
        clear();
        printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| | Q1. Which is the best    |   |\n| | programming language?    |   |\n| |                          |   |\n| | 1) JAVASCRIPT            |   |\n| | 2) C                     |   |\n| | 3) COBOL                 |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
        option = getch();
        clear();
        if (option == '1')
        {
            printf("YAY CORRECT ANSWER, THERE WILL BE NEXT QUESTION, BUT FOR NOW, ONLY 1 QUESTION\n");
            printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| | Thanks for purchasing!   |   |\n| |                          |   |\n| | Collect your Item down   |   |\n| | below                    |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
            sleep(2);
            return;
        }
        else
        {
            printf("----------------------------------\n|         The C Vending          |\n|            Machine             |\n|--------------------------------|\n| ----------------------------   |\n| |  Oh No! You chose the    |   |\n| |  wrong answer!           |   |\n| |                          |   |\n| |   Correct Answer:        |   |\n| |    a) JAVASCRIPT         |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| |                          |   |\n| ----------------------------   |\n|                                |\n|        -----------------       |\n|        |    ^ OPEN ^   |       |\n|        |    |      |   |       |\n|        -----------------       |\n|                                |\n----------------------------------\n");
            sleep(2);
            return;
        }
    }
}